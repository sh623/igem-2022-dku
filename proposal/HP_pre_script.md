In our integrated human practice, we followed the procedure of research-design-engagement-modification.

## Recognizing the problem and designing the approach

From our literature review and after consulting our PI, Prof Linfeng Huang, we identified our stakeholders as professionals in the field and the general public.

## Consulting DKU faculty

In the initial stage of our project, we consulted Prof Huansheng Cao of DKU biology department, who suggested Alphafold2 for protein visualization. Following his advice, we reached out to Prof Floyd Beckford of DKU chemistry department, and confirmed the details of protein modeling including docking software choices and data analysis protocols.


## Engaging stakeholders
 We further consulted Dr. Neel Joshi from Northwestern University, whose lab had worked on bacterial surface display. He gave us extensive information on the multi-valency potential of the surface display method. He also reminded us that *shigella* infections often occur in underdeveloped areas, which sets economical limitations on our design. In the interview with Dr. Lin from SINOVAC Biotech Ltd., he suggested that our final design should consider safety, effectiveness, and economics. To engage with the general public, we distributed a questionnaire and received over 500 responses on what people know about antibiotic resistance and *Shigella* infection. 

 ## Modification

 To modify our design, we adopted the PATCH system by Dr. Neel Joshi, for better multivalency performance. We also changed our design logic: The drug should be ideal for the prevention of the regional spread of *Shigella* infection, acting as a blocking agent for further transmission, rather than a substitute for antibiotics. The economically-friendly delivery method is in freeze-dried powder, encapsulated by shellac. In our public questionnaire responses, a significant amount of people see *E.coli* as the main causative agent for dysentery syndrome. If we reveal our approach as probiotic-nanobody therapy using yeast and *E.coli* Nissle 1917, the probiotics can be subject to skepticism. Therefore, we took educational approaches to inform people about probiotics at both DKU and local high schools, which gained tremendous successes.

 ## Conclusion

 Our integrated human practices influenced our experimental design and the overall logic of our project. We can engage with our stakeholders and the general public, achieving satisfying outcomes. Engagement is the key to creating a project design with actual social impact, and we will carry this spirit on as we progress further into our project and future careers.
